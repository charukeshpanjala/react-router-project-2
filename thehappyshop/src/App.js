import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Component } from "react"
import Home from "./components/Home/home.js";
import ProductRoute from "./components/ProductRoute/index.js"
import DumbCartRoute from "./components/CartRoute/index.js"
import CartOperations from './CartContext/index.js';
import OrdersRoute from "./components/Orders/index.js"
import EachOrderRoute from "./components/EachOrderRoute/index.js"
import './App.css';

class App extends Component {
  render() {
    return (
      <CartOperations>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/product/:id" element={<ProductRoute />} />
            <Route path="/cart" element={<DumbCartRoute />} />
            <Route path="/orders" element={<OrdersRoute />} />
            <Route path="/orders/:id" element={<EachOrderRoute />} />
          </Routes>
        </BrowserRouter>
      </CartOperations>
    );
  }
}

export default App;
