import React from 'react';


const CartContext = React.createContext({
    itemsDetails: {},
    updateDetails: () => { },
    onClickIncrement: () => { },
    onClickDecrement: () => { },

})

class CartOperations extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            itemsDetails: {}
        }
    }

    getDataAPI = async () => {
        const response = await fetch("http://localhost:9000/getData")
        const data = await response.json()
        if (data.code === 200) {
            this.setState({ itemsDetails: (data.response.data) })
        }
    }
    componentDidMount() {
        this.getDataAPI()
    }
    updateDetails = (data) => {
        this.setState({ itemsDetails: data }, this.updateServer)
    }

    updateServer = async () => {
        await fetch("http://localhost:9000/getData", {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.itemsDetails)
        })
    }

    onClickIncrement = (id) => {
        const { itemsDetails } = this.state
        const updatedDetails = { ...itemsDetails }
        updatedDetails[id].quantity += 1
        this.updateDetails(updatedDetails)
    }

    onClickDecrement = (id) => {
        const { itemsDetails } = this.state
        const updatedDetails = { ...itemsDetails }
        updatedDetails[id].quantity -= 1
        this.updateDetails(updatedDetails)
    }
    
    render() {
        return <CartContext.Provider value={{
            itemsDetails: this.state.itemsDetails,
            updateDetails: this.updateDetails,
            onClickIncrement: this.onClickIncrement,
            onClickDecrement: this.onClickDecrement

        }}>
            {this.props.children}
        </CartContext.Provider>
    }
}

export default CartOperations;

export {
    CartContext
}