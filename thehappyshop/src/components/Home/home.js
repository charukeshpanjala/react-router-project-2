import Navbar from "../Navbar/nav.js"
import ProductCard from "../ProductCard/index.js"
import { CartContext } from "../../CartContext/index.js"
import "./home.css"

function DummyHome(props) {
    const { itemsDetails, onClickIncrement, onClickDecrement } = props
    return (<div className="home-container">
        <Navbar />
        <ul className="products-container">
            {Object.keys(itemsDetails).map(productId => {
                const product = itemsDetails[productId]
                return <ProductCard key={productId} id={productId} productDetails={product} increment={onClickIncrement} decrement={onClickDecrement} />
            })}
        </ul>
    </div>);
}

const Home = () =>
    <CartContext.Consumer>
        {(props) => <DummyHome {...props} />}
    </CartContext.Consumer>

export default Home;