import { Link } from "react-router-dom"
import logo from "../../shopLogo.png"
import "./nav.css"

function Navbar() {
    return (<nav className="navbar">
        <img src={logo} alt="logo" className="nav-logo" />
            <Link to="/" className="link-element">Home</Link>
            <Link to="/cart" className="link-element">Cart</Link>
            <Link to="/orders" className="link-element">MyOrders</Link>
    </nav>);
}

export default Navbar;