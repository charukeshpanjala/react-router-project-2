import { Component } from "react"
import Navbar from "../Navbar/nav.js"
import EachOrder from "../EachOrder/index.js"
import "./index.css"

class OrdersRoute extends Component {
    constructor(props) {
        super(props);
        this.state = { orderDetails: {} }
    }
    getOrderData = async () => {
        const response = (await fetch("http://localhost:9000/orders"))
        const orders = await response.json()
        this.setState({ orderDetails: orders })
    }
    componentDidMount() {
        this.getOrderData()
    }

    renderOrders = () => {
        if (Object.keys(this.state.orderDetails).length === 0) {
            return <h1>No Orders Placed</h1>
        } else {
            return Object.keys(this.state.orderDetails).map(eachId => {
                const eachOrder = this.state.orderDetails[eachId]
                return <EachOrder details={eachOrder} key={eachId} id={eachId} />
            })
        }
    }

    render() {
        return (<>
            <Navbar />
            <div className="orders-container">
                {this.renderOrders()}
            </div>
        </>);
    }
}

export default OrdersRoute;