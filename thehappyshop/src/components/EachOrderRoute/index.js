import { useParams } from "react-router-dom";
import { Component } from "react"
import Navbar from "../Navbar/nav.js"
import "./index.css"

class DummyRoute extends Component {
    constructor(props) {
        super(props);
        this.state = { orderDetails: {} }
    }

    getOrderDetails = async () => {
        const response = (await fetch(`http://localhost:9000/orders/${this.props.id}`))
        const data = await response.json()
        this.setState({ orderDetails: data })
    }

    componentDidMount() {
        this.getOrderDetails()
    }

    render() {
        return (
            <>
                <Navbar />
                <ul className="eachOrder-container">
                    {Object.keys(this.state.orderDetails).length === 0 ? <h1>No Orders</h1> :
                        <>
                            <h1 className="order-heading" >Order Id: {this.props.id}</h1>
                            {Object.keys(this.state.orderDetails.items).map(eachItem => {
                                const details = this.state.orderDetails.items[eachItem]
                                return <div key={eachItem}><li className="cart-item" >
                                    <img src={details.imgUrl} className="cart-item-image" alt="Ice-cream" />
                                    <h1 className="cart-item-name">{details.name}</h1>
                                    <p className="cart-item-price">Qty: {details.quantity}</p>
                                    <p className="cart-item-price">Rs. {details.price * details.quantity}</p>
                                </li>
                                    <hr className="line" />
                                </div>
                            })}
                            <li className="total-container">
                                <span className="total">Total:</span>
                                <span className="total">Rs.{this.state.orderDetails.totalPrice}</span>
                            </li>
                        </>
                    }
                </ul>
            </>)
    }
}

function EachOrderRoute() {
    const { id } = useParams();
    return (<DummyRoute id={id} />);
}

export default EachOrderRoute;