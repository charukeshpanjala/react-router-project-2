import { Link } from "react-router-dom"
import "./index.css"

function EachOrder(props) {
    const { details, id } = props;
    return (<Link className="order-link" to={`/orders/${id}`}>
        <ul className="eachOrder">
            <li className="order-heading">Order Id : {id}</li>
            {Object.keys(details.items).map(eachId => {
                const eachItem = details.items[eachId]
                return (
                    <li className="eachItem" key={eachId}>
                        <p className="eachItem-name">{eachItem.name}</p>
                        <p className="eachItem-name">x{eachItem.quantity}</p>
                        <p className="eachItem-name">Rs.{eachItem.quantity * eachItem.price}</p>
                    </li>
                )
            })}
            <li className="order-total">Total Paid : <span>{details.totalPrice}</span></li>
        </ul>
    </Link>)
}

export default EachOrder;