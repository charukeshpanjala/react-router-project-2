import { Link } from 'react-router-dom'
import "./index.css";

function ProductCard(props) {
    const { productDetails, id, increment, decrement } = props;
    const { quantity } = productDetails
    
    const onClickPlus = () => {
        increment(id)
    }
    const onClickMinus = () => {
        decrement(id)
    }
    const bgColor = quantity === 0 ? "product-card" : "product-card bg-green"

    return (
        <li className={bgColor}>
            <Link to={`/product/${id}`} >
                <img src={productDetails.imgUrl} alt="Product" className="product-image" />
            </Link>
            <div className="product-details-container">
                <p className="product-name">{productDetails.name}</p>
                <p className="product-description">A rich, sweet, creamy frozen food made from a frozen mixture of milk products and flavorings.</p>
                <p className="product-price">Price: INR {productDetails.price}
                </p>
                {quantity === 0 ? <button className="add-to-cart-btn" onClick={onClickPlus}>Add to Cart</button> : <div className="quatity-control-container">
                    <button className="quatity-control-btn" onClick={onClickMinus}>-</button>
                    <span className="quatity">{quantity}</span>
                    <button className="quatity-control-btn" onClick={onClickPlus}>+</button>
                </div>}
            </div>
        </li>);
}

export default ProductCard;