import "./index.css"

function CartItem(props) {
    const { details, id, increment, decrement } = props
    const onClickPlus = () => {
        increment(id)
    }
    const onClickMinus = () => {
        decrement(id)
    }
    return (<><li className="cart-item">
        <img src={details.imgUrl} className="cart-item-image" alt="Ice-cream" />
        <h1 className="cart-item-name">{details.name}</h1>
        <div className="button-container">
            <button className="plus-btn" onClick={onClickMinus}>-</button>
            <span className="cart-quatity">{details.quantity}</span>
            <button className="plus-btn" onClick={onClickPlus} >+</button>
        </div>
        <p className="cart-item-price">Rs. {details.price * details.quantity}</p>
    </li>
        <hr className="line" />
    </>);
}

export default CartItem;