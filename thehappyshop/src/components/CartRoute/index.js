import Navbar from "../Navbar/nav.js";
import CartItem from "../CartItem/index.js";
import { CartContext } from "../../CartContext/index.js";
import { useNavigate } from 'react-router-dom';
import "./index.css"

function CartRoute(props) {
    const { itemsDetails, updateDetails, onClickIncrement, onClickDecrement } = props;
    const Navigate = useNavigate();
    const onClickRemoveAll = () => {
        const updatedDetails = Object.values(itemsDetails).map((key) => ({ ...key, quantity: 0 }))
        updateDetails(updatedDetails)
    }

    const selectedItems = Object.keys(itemsDetails).reduce((acc, itemId) => {
        const item = itemsDetails[itemId]
        if (item.quantity > 0) {
            acc.count += 1
            acc.items[itemId] = { ...item }
            acc.totalPrice += item.quantity * item.price
        }
        return acc
    }, { count: 0, items: {}, totalPrice: 0 })

    const onClickOrder = async () => {
        let convertedData = JSON.stringify(selectedItems)
        await fetch("http://localhost:9000/orders", {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: convertedData
        })
        onClickRemoveAll();
        alert("Order placed successfully");
    }

    const listItems = selectedItems.count === 0 ? <li className="no-items">No Items Selected</li> : <>
        <button className="remove-all" onClick={onClickRemoveAll}>Remove All</button>
        {
            Object.keys(selectedItems.items).map(eachId => {
                const item = selectedItems.items[eachId]
                return (<CartItem details={item} key={eachId} id={eachId} increment={onClickIncrement} decrement={onClickDecrement} />)
            })
        }
        <li className="total-container">
            <span className="total">Total:</span>
            <span className="total">Rs.{selectedItems.totalPrice}</span>
        </li>
    </>

    return <div className="cart">
        <Navbar />
        <div className="cart-container">
            <h1 className="cart-heading">My Shopping Cart</h1>
            <ul className="cart-item-container">
                {listItems}
            </ul>
            {selectedItems.count > 0 && <button className="placeOrder-button" onClick={onClickOrder}>Place Order</button>}
        </div>
    </div>;
}


const DumbCartRoute = () =>
    <CartContext.Consumer>
        {(props) => <CartRoute {...props} />}
    </CartContext.Consumer>

export default DumbCartRoute;