import { useParams } from "react-router-dom";
import Navbar from "../Navbar/nav.js"
import logo from "../../THE.png";
import { CartContext } from "../../CartContext/index.js"
import "./index.css";

const DummProductroute = (props) => {
    const { itemsDetails, onClickIncrement, onClickDecrement } = props;
    const { id } = useParams();
    const selectedItem = itemsDetails[id]
    const increment = () => {
        onClickIncrement(id)
    }
    const decrement = () => {
        onClickDecrement(id)
    }
    return (
        <div className="product-id-container">
            <Navbar />
            {selectedItem && <>
                <img src={logo} alt="Logo" className="product-details-page-logo" />
                <div className="product-route-details-container">
                    <div className="product-name-price">
                        <h1 className="product-name">{selectedItem.name}</h1>
                        <p className="product-price">Price: INR {selectedItem.price}</p>
                    </div>
                    <img src={selectedItem.imgUrl} alt="Product" className="product-img" />
                    {selectedItem.quantity === 0 ? <button className="add-to-cart-btn" onClick={increment}>Add to Cart</button> : <div className="quatity-control-container">
                        <button className="quatity-control-btn" onClick={decrement}>-</button>
                        <span className="quatity">{selectedItem.quantity}</span>
                        <button className="quatity-control-btn" onClick={increment} >+</button>
                    </div>}
                </div>
            </>}
        </div>
    )
}

const ProductRoute = () => {
    return <CartContext.Consumer>
        {props => {
            return <DummProductroute {...props} />
        }}
    </CartContext.Consumer>
}

export default ProductRoute