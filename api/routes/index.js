var express = require('express');
var router = express.Router();
const fs = require('fs')
const path = require('path');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/products/:id', function (req, res) {
  const { id } = req.params
  fs.readFile(path.resolve("products.json"), "utf8", (err, data) => {
    if (err) {
      console.log(err)
    } else {
      const updated = JSON.parse(data)
      res.send(updated.response.data[id]);
    }
  })
})

module.exports = router;
