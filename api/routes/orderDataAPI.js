var express = require('express');
var router = express.Router();
const fs = require('fs')
const path = require('path');

router.get('/', async function (req, res) {
    res.sendFile(path.resolve("orders.json"));
})

router.get('/:id', function (req, res) {
    const { id } = req.params
    fs.readFile(path.resolve("orders.json"), "utf8", (err, data) => {
        if (err) {
            console.log(err)
        } else {
            const updated = JSON.parse(data)
            res.send(updated[id]);
        }
    })
})

router.post('/', async function (req, res) {
    const data = await JSON.parse(await fs.promises.readFile(path.resolve("orders.json")))
    const dataArray = Object.keys(data)
    data[dataArray.length] = req.body

    fs.writeFile(path.resolve("orders.json"), JSON.stringify(data), "utf8", (err) => {
        if (err) {
            console.log(err)
        } else {
            res.send("success")
        }
    })
})

module.exports = router;