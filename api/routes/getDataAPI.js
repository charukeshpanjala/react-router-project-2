var express = require('express')
var router = express.Router();
const fs = require('fs')
const path = require('path');


router.get('/',async function (req, res) {
    const data = await JSON.parse(await fs.promises.readFile(path.resolve("products.json")))
    res.send(data);
})


router.post('/', async function (req, res) {
    const data = await JSON.parse(await fs.promises.readFile(path.resolve("products.json")))
    data.response.data = req.body
    fs.writeFile(path.resolve("products.json"), JSON.stringify(data), "utf8", (err) => {
        if (err) {
            console.log(err)
        } else {
            res.send("success")
        }
    })
  })

module.exports = router;